INTRODUCTION
------------

This module provides additional UUID-based Services (see
http://drupal.org/project/uuid and http://drupal.org/project/services) for
taxonomy terms and vocabularies, including the ability to reorder terms on a
remote site.

Reordering terms via Services may be useful if you are using Deployment
(http://drupal.org/project/deploy) to sync taxonomy terms between multiple
sites and want to ensure that reordering the terms via drag-and-drop on the
source site propagates those changes to the target sites.

This is an API module only; there is no user interface, and therefore no reason
to install this module unless you are using it in code you are writing or
another module's installation instructions tell you to install it.

EXAMPLES
--------

To use this module to reorder taxonomy terms on a target site whenever they are
changed via drag-and-drop on a source site, it may be helpful to use the Deploy
Services Client module (http://drupal.org/project/deploy_services_client).

In that case, install this module on both sites and install Deploy Services
Client on the source site. Then, use code like the following in a custom
module:

<?php
/**
 * Implements hook_form_FORM_ID_alter().
 */
function MYMODULE_form_taxonomy_overview_terms_alter(&$form, &$form_state) {
  $form['#submit'][] = 'MYMODULE_taxonomy_overview_terms_submit';
}

/**
 * Custom submit handler for the form that reorders taxonomy terms.
 */
function MYMODULE_taxonomy_overview_terms_submit($form, &$form_state) {
  // The first section just deals with annoying issues specific to the taxonomy
  // overview form. It's necessary for the code to work, but not very
  // illustrative.

  // When the "Reset to alphabetical" button is clicked, we might be displaying
  // a confirmation form rather than actually doing the reordering. Catch that
  // case and bail out when it happens.
  if ($form_state['triggering_element']['#value'] == t('Reset to alphabetical') && $form_state['values']['reset_alphabetical'] !== TRUE) {
    return;
  }
  // Get the vocabulary ID. This can be stored in different places depending on
  // which stage of the form submission we're at.
  if (isset($form['#vocabulary'])) {
    $vid = $form['#vocabulary']->vid;
  }
  elseif (isset($form_state['values']['vid'])) {
    $vid = $form_state['values']['vid'];
  }
  // Bail out if we don't recognize this form.
  else {
    return;
  }

  // The interesting code is below. You'd probably run it only after checking
  // that the vocabulary identified by $vid is actually one you want to
  // propagate to the target site.

  // Force a static cache reset before propagating the changes, since the
  // submit handlers which ran before this one may have updated things in the
  // database without clearing static caches.
  taxonomy_terms_static_reset();
  // Now propagate the changes. Here, the Deployment endpoint you would load is
  // the one associated with the desired target site.
  try {
    $terms = uuid_taxonomy_services_get_tree($vid);
    $endpoint = deploy_endpoint_load('your_endpoint_name');
    $client = new DeployServicesClient($endpoint);
    $client->login();
    $client->request('taxonomy_term/reorder_terms', 'POST', drupal_json_encode($terms));
    $client->logout();
  }
  catch (Exception $e) {
    watchdog_exception('MYMODULE', $e);
  }
}
?>

CREDITS
-------

This project was sponsored by Advomatic (http://advomatic.com).
